package com.company.logic.controller

import groovy.util.logging.Log4j
import internal.pnut.context.ContextConfig

import javax.ws.rs.GET
import javax.ws.rs.Path

/**
 * Created by Claudio Carcaci on 27 May 2017.
 */

@Log4j
@Path("admin")
class Admin {
	@GET
	@Path("reloadConfig")
	public void reloadConfig() {
		ContextConfig.loadConfig()
	}
}
