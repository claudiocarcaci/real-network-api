package com.company.logic.controller

import com.company.logic.model.Bar
import groovy.util.logging.Log4j
import internal.pnut.main.InternalConfig
import internal.pnut.securing.SecureJwt

import javax.ws.rs.GET
import javax.ws.rs.Path
import javax.ws.rs.Produces
/**
 * Created by Claudio Carcaci on 29 Sep 2016.
 */

@Log4j
@Path("foo")
class Foo {
	@GET
	@Path("fooAuth")
	@Produces(InternalConfig.jsonContentHeader)
	@SecureJwt
	// Use always Map or List types because Marshaller (filter) needs this type to convert data into Json
	public Map fooAuth() {
		log.info("foo auth")

		return [ auth: "Ok auth" ]
	}

	@GET
	@Path("fooUnauth")
	@Produces(InternalConfig.jsonContentHeader)
	// Use always Map or List types because Marshaller (filter) needs this type to convert data into Json
	public List fooUnauth() {
		log.info("foo unauth")

		return [ "Ok unauth" ]
	}

	@GET
	@Path("bar")
	@Produces(InternalConfig.jsonContentHeader)
	public Bar bar() {
		return new Bar()
	}
}
