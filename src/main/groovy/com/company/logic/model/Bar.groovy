package com.company.logic.model

import internal.pnut.jerseys.Marshallable

/**
 * Created by utente on 09/02/17.
 */

class Bar implements Marshallable {
	private static String content = "This is a class"

	@Override
	public Map picture() {
		return [ barContent: content ]
	}
}
