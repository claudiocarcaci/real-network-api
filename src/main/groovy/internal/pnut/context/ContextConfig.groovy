package internal.pnut.context

/**
 * Created by Claudio Carcaci on 27 Sep 2016.
 */
class ContextConfig {
	private static Map contextualConfigs = null

	public static Map loadConfig() {
		String configFileName = System.getProperty(CONFIGFILEPARAM)

		if (configFileName != null) {
			return new ConfigSlurper().parse(new File(configFileName).toURI().toURL())
		} else {
			throw new Exception("Error, missed configuration file, use -D" + CONFIGFILEPARAM + "=<config file path>")
		}
	}

	public static Map getValues() {
		Map loadedConfigs = null

		if(contextualConfigs == null) {
			synchronized (this) {
				if(loadedConfigs == null) {
					try {
						loadedConfigs = loadConfig()
					} catch(Exception e) {
						System.out.println("${e.getMessage()}")

						return null
					}
				}
			}

			contextualConfigs = loadedConfigs
		}

		return contextualConfigs
	}

	private static final String CONFIGFILEPARAM = "contextconfigfile"
}
