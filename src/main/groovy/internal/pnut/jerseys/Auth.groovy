package internal.pnut.jerseys

import internal.pnut.main.InternalConfig
import groovy.util.logging.Log4j
import internal.pnut.securing.JwtAuthenticationException
import internal.pnut.securing.UserAuthenticator

import javax.ws.rs.*
/**
 * Created by Claudio Carcaci on 27 Sep 2016.
 */
@Log4j
@Path("auth")
class Auth {
	@POST
	@Path("register")
	@Produces(InternalConfig.jsonContentHeader)
	public Map register(@FormParam("username")String username, @FormParam("sha256Password")String sha256Password) {
		String result
		UserAuthenticator userAuthenticator = new UserAuthenticator()

		result = userAuthenticator.createUser(username, sha256Password)

		return [ result: result ]
	}

	@POST
	@Path("refresh")
	@Produces(InternalConfig.jsonContentHeader)
	public Map refresh(@HeaderParam("jwt")String token, @FormParam("username")String username, @FormParam("password")String password) {
		String result
		UserAuthenticator userAuthenticator = new UserAuthenticator()

		result = userAuthenticator.refreshToken(username, password, token)

		return [ result: result ]
	}

	@POST
	@Path("login")
	@Produces(InternalConfig.jsonContentHeader)
	public Map login(@FormParam("username")String username, @FormParam("password")String password) {
		log.info("Trying to authenticate user $username using password $password")
		UserAuthenticator userAuthenticator = new UserAuthenticator()
		String result

		try {
			result = userAuthenticator.generateJwtToken(username, password)
		} catch(JwtAuthenticationException e) {
			result = "Authentication is not valid"
		}

		return [ jwt: result ]
	}
}
