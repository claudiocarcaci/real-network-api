package internal.pnut.jerseys

import groovy.json.JsonBuilder
import groovy.util.logging.Log4j

import javax.ws.rs.container.ContainerRequestContext
import javax.ws.rs.container.ContainerResponseContext
import javax.ws.rs.container.ContainerResponseFilter
import javax.ws.rs.ext.Provider
/**
 * Created by Claudio Carcaci on 8 Feb 2017.
 */
@Provider
@Log4j
// Marshall Map or List return data from Controllers (Jerseys) into Json
class Marshaller implements ContainerResponseFilter {
	@Override
	void filter(ContainerRequestContext requestContext, ContainerResponseContext responseContext) throws IOException {
		JsonBuilder jsonBuilder
		Object payload = responseContext.getEntity()

		if(payload instanceof Map || payload instanceof List) {
			jsonBuilder = new JsonBuilder(payload)

			responseContext.setEntity(jsonBuilder.toPrettyString())
		} else if(payload instanceof Marshallable) {
			jsonBuilder = new JsonBuilder(((Marshallable)payload).picture())

			responseContext.setEntity(jsonBuilder.toPrettyString())
		}
	}
}
