package internal.pnut.jerseys

import javax.ws.rs.NotAuthorizedException
import javax.ws.rs.core.Response
import javax.ws.rs.ext.ExceptionMapper
import javax.ws.rs.ext.Provider

/**
 * Created by Claudio Carcaci on 28 Sep 2016.
 */
@Provider
class NotAuthorizedMapper implements ExceptionMapper<NotAuthorizedException> {
	@Override
	Response toResponse(NotAuthorizedException exception) {
		return Response.status(401).entity(exception.getMessage()).type("text/plain").build()
	}
}
