package internal.pnut.jerseys

import javax.ws.rs.NotFoundException
import javax.ws.rs.core.Response
import javax.ws.rs.ext.ExceptionMapper
import javax.ws.rs.ext.Provider
/**
 * Created by Claudio Carcaci on 28 Sep 2016.
 */
@Provider
class NotFoundMapper implements ExceptionMapper<NotFoundException> {
	@Override
	public Response toResponse(NotFoundException exception) {
		return Response.status(404).entity(exception.getMessage()).type("text/plain").build()
	}
}
