package internal.pnut.jerseys

import javax.ws.rs.ServerErrorException
import javax.ws.rs.core.Response
import javax.ws.rs.ext.ExceptionMapper
import javax.ws.rs.ext.Provider

/**
 * Created by Claudio Carcaci on 28 Sep 2016.
 */
@Provider
class ServerErrorMapper implements ExceptionMapper<ServerErrorException> {
	@Override
	Response toResponse(ServerErrorException exception) {
		return Response.status(500).entity(exception.getMessage()).type("text/plain").build()
	}
}
