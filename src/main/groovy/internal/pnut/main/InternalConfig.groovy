package internal.pnut.main

/**
 * Created by Claudio Carcaci on 29 Sep 2016.
 */
class InternalConfig {
	public static final String resourcePackage = "internal.pnut.jerseys"
	public static final String securityFilterPackage = "internal.pnut.securing"
	public static final String jsonContentHeader = "application/json; charset=utf-8"
}
