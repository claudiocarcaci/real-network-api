package internal.pnut.main

import internal.pnut.context.ContextConfig
import groovy.util.logging.Log4j
import org.apache.log4j.PropertyConfigurator
import org.glassfish.grizzly.http.server.HttpServer
import org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpServerFactory
import org.glassfish.jersey.server.ResourceConfig
/**
 * Created by Claudio Carcaci on 26 Sep 2016.
 */
@Log4j
class StartPnut {
	public static void main(String[] args) throws IOException {
		// Log4j configurations injection
		// This path MUST be fixed
		PropertyConfigurator.configure("src/resources/log4j.properties")

		String serverUri = "${ContextConfig.getValues().server.host}:${ContextConfig.getValues().server.port}"
		String resourcePackage = InternalConfig.resourcePackage
		String securingPackage = InternalConfig.securityFilterPackage
		String logicPackage = ContextConfig.getValues().apiLogicPackageBase

		ResourceConfig resources = new ResourceConfig().packages(resourcePackage, securingPackage, logicPackage)

		HttpServer httpServer = GrizzlyHttpServerFactory.createHttpServer(new URI("$serverUri"), resources)
		httpServer.start()

		log.info("Server starts at $serverUri")
	}
}
