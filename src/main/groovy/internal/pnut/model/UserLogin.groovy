package internal.pnut.model

/**
 * Created by Claudio Carcaci on 27 Sep 2016.
 */
class UserLogin {
	public String username
	public String sha256Password
	public String jwtToken

	// Todo link to DB
	private static List<UserLogin> users =  []

	public boolean persist() {
		// Todo insert into DB
		users << this
		return true
	}

	public static getByUsername(String username) {
		// Todo get from DB
		UserLogin userLogin = users.find { it.username == username }

		return userLogin
	}
}
