package internal.pnut.securing

/**
 * Created by Claudio Carcaci on 25 Aug 2016.
 */
class JwtAuthenticationException extends Exception {
	public JwtAuthenticationException(String message) {
		super(message)
	}
}
