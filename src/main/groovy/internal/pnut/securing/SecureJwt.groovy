package internal.pnut.securing

import javax.ws.rs.NameBinding
import java.lang.annotation.Retention
import java.lang.annotation.RetentionPolicy
/**
 * Created by Claudio Carcaci on 25 Aug 2016.
 */

@NameBinding
@Retention(RetentionPolicy.RUNTIME)
// @Target([ElementType.METHOD, ElementType.TYPE])
public @interface SecureJwt {

}
