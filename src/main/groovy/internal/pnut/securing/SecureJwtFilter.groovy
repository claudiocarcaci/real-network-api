package internal.pnut.securing

import internal.pnut.context.ContextConfig
import groovy.util.logging.Log4j

import javax.ws.rs.container.ContainerRequestContext
import javax.ws.rs.container.ContainerRequestFilter
import javax.ws.rs.core.Response
import javax.ws.rs.ext.Provider

/**
 * Created by Claudio Carcaci on 29 Sep 2016.
 */
// Bind filter with @SecureJwt annotation
@SecureJwt
// Let Jersey to autodiscover this Filter
@Provider
@Log4j
class SecureJwtFilter implements ContainerRequestFilter {
	@Override
	void filter(ContainerRequestContext requestContext) throws IOException {
		String jwtTokenHeader = ContextConfig.getValues().jwt.tokenHeader
		String jwtAuth = requestContext.getHeaderString(jwtTokenHeader)
		UserAuthenticator userAuthenticator = new UserAuthenticator()

		try {
			if (!userAuthenticator.verifyAccess(jwtAuth)) {
				requestContext.abortWith(Response.status(Response.Status.UNAUTHORIZED)
					.entity("Jwt token is not authenticated")
					.build())
			}
		} catch(JwtAuthenticationException e) {
			log.error("Jwt token is invalid, ${e.getMessage()}")
			requestContext.abortWith(Response.status(Response.Status.ACCEPTED)
				.entity("Jwt token has invalid format")
				.build())
		}
	}
}
