package internal.pnut.securing

import internal.pnut.context.ContextConfig
import internal.pnut.model.UserLogin
import io.jsonwebtoken.*

import javax.crypto.spec.SecretKeySpec
import javax.xml.bind.DatatypeConverter
import java.nio.charset.StandardCharsets
import java.security.Key
import java.security.MessageDigest

/**
 * Created by Claudio Carcaci on 27 Sep 2016.
 */
class UserAuthenticator {
	public String createUser(String username, String sha256Password) {
		UserLogin userLogin = new UserLogin(username: username, sha256Password: sha256Password)

		if(userLogin.persist()) {
			return "Ok"
		} else {
			return "Error"
		}
	}

	public String generateJwtToken(String username, String password) throws JwtAuthenticationException {
		UserLogin userLogin = UserLogin.getByUsername(username)

		if(userLogin != null) {
			// Verify if password is right
			MessageDigest digest = MessageDigest.getInstance("SHA-256")
			String sha256Digest = digest.digest(password.getBytes(StandardCharsets.UTF_8)).encodeBase64()

			String userLoginSha256Password = userLogin.sha256Password

			if(sha256Digest == userLoginSha256Password) {
				Long ttl = (Long) ContextConfig.getValues().jwt.tokenTtl
				String appKey = (String) ContextConfig.getValues().jwt.authenticationKey
				String issuer = (String) ContextConfig.getValues().jwt.issuer

				SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS256
				Date now = new Date()

				Date expiringDate = new Date(now.getTime() + ttl)

				byte[] secretAppKeyBytes = DatatypeConverter.parseBase64Binary(appKey)
				Key signingKey = new SecretKeySpec(secretAppKeyBytes, signatureAlgorithm.getJcaName())

				JwtBuilder builder = Jwts.builder().
					setId(generateJwtId(username, sha256Digest)).
					setIssuedAt(now).
					setIssuer(issuer).
					setSubject(username).
					signWith(signatureAlgorithm, signingKey)

				builder.setExpiration(expiringDate)

				String jwtName = ContextConfig.getValues().jwt.tokenHeader
				String jwtToken = builder.compact()
				userLogin.jwtToken = jwtToken

				userLogin.persist()

				return jwtToken
			} else {
				throw new JwtAuthenticationException("Wrong password for username $username")
			}
		} else {
			throw new JwtAuthenticationException("Unable to find username $username")
		}
	}

	public String refreshToken(String username, String password, String jwt) throws JwtAuthenticationException {
		String error
		String appKey = (String)ContextConfig.getValues().jwt.authenticationKey
		byte[] secretAppKeyBytes = DatatypeConverter.parseBase64Binary(appKey)

		try {
			Jwts.parser().setSigningKey(secretAppKeyBytes).parseClaimsJws(jwt).getBody()
		} catch(ExpiredJwtException e) {
			return generateJwtToken(username, password)
		} catch(UnsupportedJwtException | MalformedJwtException | IllegalArgumentException e) {
			error = e.getMessage()

			return error
		}

		error = "token still alive"

		return error
	}

	public boolean verifyAccess(String jwt) throws JwtAuthenticationException {
		String appKey = (String)ContextConfig.getValues().jwt.authenticationKey
		byte[]  secretAppKeyBytes = DatatypeConverter.parseBase64Binary(appKey)
		Claims claims

		try {
			claims = Jwts.parser().setSigningKey(secretAppKeyBytes).parseClaimsJws(jwt).getBody()
		} catch(ExpiredJwtException e) {
			throw new JwtAuthenticationException("Token expired $jwt ${e.getMessage()}")
		} catch(SignatureException e) {
			throw new JwtAuthenticationException("Invalid signature $jwt ${e.getMessage()}")
		} catch(UnsupportedJwtException | MalformedJwtException | IllegalArgumentException e) {
			throw new JwtAuthenticationException("Token has invalid format $jwt ${e.getMessage()}")
		}

		String jwtUsername = claims.getSubject()
		UserLogin userLogin = UserLogin.getByUsername(jwtUsername)

		if(userLogin != null) {
			String jwtId = claims.getId()
			String authId = generateJwtId(userLogin.username, userLogin.sha256Password)

			return jwtId == authId && jwt == userLogin.jwtToken
		} else {
			throw new JwtAuthenticationException("Unable to find username $jwtUsername")
		}
	}

	public static generateJwtId(String username, String sha256Password) {
		String seed = "$username$sha256Password"
		MessageDigest digest = MessageDigest.getInstance("SHA-256")
		String sha256Hash = digest.digest(seed.getBytes(StandardCharsets.UTF_8)).encodeBase64()

		return sha256Hash
	}
}
