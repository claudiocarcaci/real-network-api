jwt {
	tokenTtl = 86400000 // One day in millis
	authenticationKey = "mgumjsmc8ule1gshb1i7cx5a"
	issuer = "Real-network"
	tokenHeader = "jwt"
}

server {
	host = "http://localhost"
	port = 8080
}

log4j {
	logPath = "/log"
}

apiLogicPackageBase = "com.company.logic"
